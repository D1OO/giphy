package net.shvdy.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class AllGifsForQueryDto {
    private String query;
    private Set<String> gifPaths;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllGifsForQueryDto that = (AllGifsForQueryDto) o;
        return getQuery().equals(that.getQuery());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuery());
    }
}
