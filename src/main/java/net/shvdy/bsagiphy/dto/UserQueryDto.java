package net.shvdy.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 06.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class UserQueryDto {
    private String userId;
    private String query;
}
