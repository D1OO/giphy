package net.shvdy.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryInBodyDto {
    @Pattern(regexp = "^[^<>%$?*:\"|/\\\\]*$")
    private String query;
}
