package net.shvdy.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
@Builder
public class UserHistoryRecordDto {
    private String date;
    private String query;
    private String gif;
}
