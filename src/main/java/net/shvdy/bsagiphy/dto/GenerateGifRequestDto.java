package net.shvdy.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.Pattern;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@AllArgsConstructor
@Getter
public class GenerateGifRequestDto {
    @Pattern(regexp = "^[^<>%$?*:\"|/\\\\]*$")
    private String query;
    private boolean force;
}
