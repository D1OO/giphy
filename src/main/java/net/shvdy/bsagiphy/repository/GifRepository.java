package net.shvdy.bsagiphy.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Repository
public class GifRepository {
    @Value("${users.dir}")
    private String usersDirName;

    public Set<String> findAll() throws IOException {
        Set<String> allGifs = new HashSet<>();
        Path usersDir = Path.of(usersDirName);

        try (Stream<Path> userFolders = Files.walk(usersDir)) {
            userFolders.filter(q -> Files.isDirectory(q) && !q.equals(usersDir))
                    .forEach(userDir -> {
                        try (Stream<Path> queryDir = Files.walk(userDir)) {
                            queryDir.filter(qd -> Files.isDirectory(qd) && !qd.equals(userDir))
                                    .forEach(qd -> allGifs.addAll(getAllGifNames(qd)));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
        return allGifs;
    }

    private Set<String> getAllGifNames(Path queryDir) {
        try (Stream<Path> queryGifs = Files.walk(queryDir)) {
            return queryGifs.filter(Files::isRegularFile)
                    .map(this::formResourcePath)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String formResourcePath(Path path) {
        return path.subpath(1, path.getNameCount()).toString().replace("\\", "/");
    }
}