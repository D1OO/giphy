package net.shvdy.bsagiphy.repository;

import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ResourceRepository {
    Random rnd = new Random();

     default String formResourcePath(Path path) {
        return path.subpath(1, path.getNameCount()).toString().replace("\\", "/");
    }

    default Set<AllGifsForQueryDto> findAll(Path folder) throws IOException {
        Set<AllGifsForQueryDto> data = new HashSet<>();
        try (Stream<Path> queries = Files.walk(folder)) {
            queries.filter(q -> Files.isDirectory(q) && !q.equals(folder))
                    .forEach(queryDir -> {

                        Set<String> allQueryGifs;
                        try (Stream<Path> queryGifs = Files.walk(queryDir)) {
                            allQueryGifs = queryGifs.filter(Files::isRegularFile)
                                    .map(this::formResourcePath)
                                    .collect(Collectors.toSet());
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        data.add(new AllGifsForQueryDto(queryDir.getFileName().toString(), allQueryGifs));
                    });
        }
        return data;
    }

    default Optional<Path> findRandomForQuery(Path path) throws IOException {
        if (path.toFile().exists()) {
            int totalGifsCount = (int) Files.list(path).count();
            int randomGifIndex = rnd.nextInt(totalGifsCount);
            return Files.list(path).skip(randomGifIndex).findFirst();
        } else {
            return Optional.empty();
        }
    }

    default void cleanAll(Path dir) throws IOException {
        if (dir.toFile().exists()) {
            Files.walk(dir)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }
}
