package net.shvdy.bsagiphy.repository;

import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 06.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Repository
public class GifCacheRepository implements ResourceRepository {
    @Value("${cache.dir}")
    private String cacheRootDir;

    public Optional<Path> findRandomForQuery(String query) throws IOException {
        return findRandomForQuery(Path.of(cacheRootDir + "/" + query));
    }

    public Path saveFromUrl(String query, URL urlOnGiphy) throws IOException {
        Path path = Path.of(String.format("%s/%s/%s", cacheRootDir, query, urlOnGiphy.getPath()));
        File savedGif = path.toFile();
        if (!savedGif.exists()) {
            savedGif.getParentFile().mkdirs();
            try (InputStream in = urlOnGiphy.openStream();
                 FileOutputStream out = new FileOutputStream(savedGif)) {

                out.getChannel().transferFrom(Channels.newChannel(in), 0, Long.MAX_VALUE);
            }
        }
        return path;
    }

    public Set<AllGifsForQueryDto> findAll(String query) throws IOException {
        Path cahcedQueryFolder = Path.of(String.format("%s/%s", cacheRootDir, query));
        Set<String> allQueryGifs;
        try (Stream<Path> queryGifs = Files.walk(cahcedQueryFolder)) {
            allQueryGifs = queryGifs.filter(Files::isRegularFile)
                    .map(this::formResourcePath)
                    .collect(Collectors.toSet());
        }
        return Set.of(new AllGifsForQueryDto(query, allQueryGifs));
    }

    public Set<AllGifsForQueryDto> findAll() throws IOException {
        return findAll(Path.of(cacheRootDir));
    }

    public void cleanAll() throws IOException {
        cleanAll(Path.of(cacheRootDir));
    }

}
