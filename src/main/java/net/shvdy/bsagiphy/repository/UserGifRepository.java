package net.shvdy.bsagiphy.repository;

import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import net.shvdy.bsagiphy.dto.UserHistoryRecordDto;
import net.shvdy.bsagiphy.dto.UserQueryDto;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Repository
public class UserGifRepository implements ResourceRepository {
    private static final String HISTORY_FILE = "history.csv";
    private static final String[] HISTORY_FILE_HEADERS = {"date", "query", "gif"};
    @Value("${users.dir}")
    private String usersDir;

    public Path formGifPath(String userId, String query, String fileName) {
        return Path.of(String.format("%s/%s/%s/%s", usersDir, userId, query, fileName));
    }

    public boolean checkIsPresent(Path path) {
        return path.toFile().exists();
    }

    public Optional<String> findRandomForQuery(UserQueryDto userQueryDto) throws IOException {
        return findRandomForQuery(Path.of(String.format(
                "%s/%s/%s", usersDir, userQueryDto.getUserId(), userQueryDto.getQuery())))
                .map(this::formResourcePath);
    }

    public Path saveFromPath(String userId, String query, Path paths) throws IOException {
        Path path = Path.of(String.format("%s/%s/%s/%s", usersDir, userId, query, paths.getFileName()));
        File savedGif = path.toFile();
        if (!savedGif.exists()) {
            savedGif.getParentFile().mkdirs();
            Files.copy(paths, path, StandardCopyOption.REPLACE_EXISTING);
        }
        return path;
    }

    public Set<AllGifsForQueryDto> getAllUserGifs(String userId) throws IOException {
        return findAll(Path.of(String.format("%s/%s", usersDir, userId)));
    }

    public void writeNewRecordToUsersHistory(String userId, String query, Path gifPath) throws IOException {
        File file = new File(String.format("%s/%s/%s", usersDir, userId, HISTORY_FILE));
        CSVPrinter printer;
        if (file.exists()) {
            printer = new CSVPrinter(new FileWriter(file, true), CSVFormat.DEFAULT);
        } else {
            printer = new CSVPrinter(new FileWriter(file), CSVFormat.DEFAULT.withHeader(HISTORY_FILE_HEADERS));
        }
        printer.printRecord(LocalDate.now().toString(), query, formResourcePath(gifPath));
        printer.close();
    }

    public Optional<List<UserHistoryRecordDto>> findHistory(String userId) throws IOException {
        File history = Path.of(String.format("%s/%s/%s", usersDir, userId, HISTORY_FILE)).toFile();
        if (history.exists()) {
            return Optional.of(readCsvFile(history));
        }
        return Optional.empty();
    }

    public void cleanHistory(String userId) {
        File file = new File(String.format("%s/%s/%s", usersDir, userId, HISTORY_FILE));
        if (file.exists()) {
            file.delete();
        }
    }

    public void cleanAll(String userId) throws IOException {
        cleanAll(Path.of(String.format("%s/%s", usersDir, userId)));
    }

    private List<UserHistoryRecordDto> readCsvFile(File file) throws IOException {
        Reader in = new FileReader(file);
        List<UserHistoryRecordDto> records = new ArrayList<>();

        Iterable<CSVRecord> historyCsvRecords = CSVFormat.DEFAULT
                .withHeader(HISTORY_FILE_HEADERS)
                .withFirstRecordAsHeader()
                .parse(in);

        historyCsvRecords.iterator().forEachRemaining(line -> {
            records.add(UserHistoryRecordDto.builder()
                    .date(line.get(HISTORY_FILE_HEADERS[0]))
                    .query(line.get(HISTORY_FILE_HEADERS[1]))
                    .gif(line.get(HISTORY_FILE_HEADERS[2])).build());
        });

        return records;
    }
}
