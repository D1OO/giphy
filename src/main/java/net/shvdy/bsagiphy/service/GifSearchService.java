package net.shvdy.bsagiphy.service;

import net.shvdy.bsagiphy.config.MemoryCache;
import net.shvdy.bsagiphy.dto.UserQueryDto;
import net.shvdy.bsagiphy.repository.UserGifRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class GifSearchService {
    private MemoryCache memoryCache;
    private UserGifRepository userGifRepository;

    public GifSearchService(MemoryCache memoryCache, UserGifRepository userGifRepository) {
        this.memoryCache = memoryCache;
        this.userGifRepository = userGifRepository;
    }

    public Optional<String> searchFromMemoryCache(UserQueryDto userQueryDto) throws IOException {
        Optional<String> path = memoryCache.get(userQueryDto.getUserId(), userQueryDto.getQuery());
        return (path.isPresent()) ? path : searchInUsersFolder(userQueryDto);
    }

    public Optional<String> searchInUsersFolder(UserQueryDto userQueryDto) throws IOException {
        Optional<String> gifPath = userGifRepository.findRandomForQuery(userQueryDto);
        gifPath.ifPresent(path -> memoryCache.put(userQueryDto.getUserId(), userQueryDto.getQuery(), path));
        return gifPath;
    }

}
