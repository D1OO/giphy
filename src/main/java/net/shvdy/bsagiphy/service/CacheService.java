package net.shvdy.bsagiphy.service;

import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import net.shvdy.bsagiphy.exception.NoGifsFoundForQueryException;
import net.shvdy.bsagiphy.exception.ReceivedEmptyResponseException;
import net.shvdy.bsagiphy.repository.GifCacheRepository;
import net.shvdy.bsagiphy.service.helper.GiphyApiHelper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class CacheService {
    GifCacheRepository gifCacheRepository;
    GiphyApiHelper giphyApiHelper;

    public CacheService(GifCacheRepository gifCacheRepository, GiphyApiHelper giphyApiHelper) {
        this.gifCacheRepository = gifCacheRepository;
        this.giphyApiHelper = giphyApiHelper;
    }

    public Set<AllGifsForQueryDto> getAll() throws IOException {
        return gifCacheRepository.findAll();
    }

    public Set<AllGifsForQueryDto> getAll(String query) throws IOException {
        return gifCacheRepository.findAll(query);
    }

    public AllGifsForQueryDto generate(String query) throws IOException, ReceivedEmptyResponseException,
            NoGifsFoundForQueryException {
        gifCacheRepository.saveFromUrl(query, giphyApiHelper.getGifURLFromGiphyApiSearch(query));
        return gifCacheRepository.findAll(query).stream().findFirst().get();
    }

    public void deleteAll() throws IOException {
        gifCacheRepository.cleanAll();
    }
}
