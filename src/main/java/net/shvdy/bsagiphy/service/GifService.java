package net.shvdy.bsagiphy.service;

import net.shvdy.bsagiphy.repository.GifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class GifService {
    @Autowired
    GifRepository gifRepository;

    public Set<String> getAll() throws IOException {
        return gifRepository.findAll();
    }
}
