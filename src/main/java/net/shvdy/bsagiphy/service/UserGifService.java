package net.shvdy.bsagiphy.service;

import net.shvdy.bsagiphy.config.MemoryCache;
import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import net.shvdy.bsagiphy.dto.UserHistoryRecordDto;
import net.shvdy.bsagiphy.repository.UserGifRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class UserGifService {
    UserGifRepository userGifRepository;
    MemoryCache memoryCache;

    public UserGifService(UserGifRepository userGifRepository, MemoryCache memoryCache) {
        this.userGifRepository = userGifRepository;
        this.memoryCache = memoryCache;
    }

    public Set<AllGifsForQueryDto> getAll(String userId) throws IOException {
        return userGifRepository.getAllUserGifs(userId);
    }

    public Optional<List<UserHistoryRecordDto>> getHistory(String userId) throws IOException {
        return userGifRepository.findHistory(userId);
    }

    public void cleanHistory(String userId) {
        userGifRepository.cleanHistory(userId);
    }

    public void cleanAll(String userId) throws IOException {
        userGifRepository.cleanAll(userId);
        memoryCache.deleteUser(userId);
    }

    public void resetCache(String userId) {
        memoryCache.deleteUser(userId);
    }

    public void resetCache(String userId, String query) {
        memoryCache.deleteUserQuery(userId, query);
    }
}
