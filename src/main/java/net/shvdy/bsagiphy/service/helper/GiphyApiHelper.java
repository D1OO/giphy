package net.shvdy.bsagiphy.service.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import net.shvdy.bsagiphy.exception.NoGifsFoundForQueryException;
import net.shvdy.bsagiphy.exception.ReceivedEmptyResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Optional;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Component
public class GiphyApiHelper {
    @Value(value = "${giphy.api.key}")
    private String giphyAPIKey;
    @Value(value = "${giphy.api.random}")
    private String randomGifEndpoint;
    @Value(value = "${giphy.files}")
    private String giphyFilesUrl;

    @Autowired
    private JsonResponseHelper jsonResponseHelper;

    public URL getGifURLFromGiphyApiSearch(String query)
            throws ReceivedEmptyResponseException, JsonProcessingException,
            NoGifsFoundForQueryException, MalformedURLException {

        String responseJson = findRandomGif(query).get().retrieve().bodyToMono(String.class)
                .blockOptional(Duration.ofSeconds(30))
                .orElseThrow(() -> new ReceivedEmptyResponseException("Received empty response"));
        String id = getGifFromGiphyResponse(responseJson)
                .orElseThrow(() -> new NoGifsFoundForQueryException("Nothing is found for \n" + query + "\n"))
                .get("id")
                .textValue();

        return new URL(String.format(giphyFilesUrl + "/%s.gif", id));
    }

    public WebClient findRandomGif(String query) {
        return WebClient.builder().baseUrl(randomGifEndpoint + "?" +
                "api_key=" + giphyAPIKey +
                "&tag=" + query).build();
    }

    public Optional<JsonNode> getGifFromGiphyResponse(String responseJsonString)
            throws JsonProcessingException {
        return Optional.ofNullable(jsonResponseHelper
                .getJsonNodeFromString(responseJsonString)
                .path("data"));
    }

}
