package net.shvdy.bsagiphy.service.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

/**
 * 05.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Component
public class JsonResponseHelper {
    private ObjectMapper jackson = new ObjectMapper();

    public JsonNode getJsonNodeFromString(String jsonString) throws JsonProcessingException {
        return jackson.readTree(jsonString);
    }
}
