package net.shvdy.bsagiphy.service;

import net.shvdy.bsagiphy.config.MemoryCache;
import net.shvdy.bsagiphy.dto.UserQueryDto;
import net.shvdy.bsagiphy.exception.NoGifsFoundForQueryException;
import net.shvdy.bsagiphy.exception.ReceivedEmptyResponseException;
import net.shvdy.bsagiphy.repository.GifCacheRepository;
import net.shvdy.bsagiphy.repository.UserGifRepository;
import net.shvdy.bsagiphy.service.helper.GiphyApiHelper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class GifGenerationService {

    private GiphyApiHelper giphyApiHelper;
    private MemoryCache memoryCache;
    private UserGifRepository userGifRepository;
    private GifCacheRepository gifCacheRepository;


    public GifGenerationService(GiphyApiHelper giphyApiHelper, MemoryCache memoryCache,
                                UserGifRepository userGifRepository, GifCacheRepository gifCacheRepository) {
        this.giphyApiHelper = giphyApiHelper;
        this.memoryCache = memoryCache;
        this.userGifRepository = userGifRepository;
        this.gifCacheRepository = gifCacheRepository;
    }

    public String generateGifFromDiskCache(UserQueryDto userQueryDto)
            throws IOException, ReceivedEmptyResponseException, NoGifsFoundForQueryException {
        Optional<Path> gifInDiskCache = gifCacheRepository.findRandomForQuery(userQueryDto.getQuery());
        if (gifInDiskCache.isEmpty()) {
            return generateGifFromApi(userQueryDto);
        }
        return finalizeGenerate(userQueryDto, gifInDiskCache.get());
    }

    public String generateGifFromApi(UserQueryDto userQueryDto)
            throws NoGifsFoundForQueryException, IOException, ReceivedEmptyResponseException {
        Path gifInDiskCache = saveToCacheFromApi(userQueryDto);
        return finalizeGenerate(userQueryDto, gifInDiskCache);
    }

    private Path saveToCacheFromApi(UserQueryDto userQueryDto)
            throws IOException, ReceivedEmptyResponseException, NoGifsFoundForQueryException {
        return gifCacheRepository
                .saveFromUrl(userQueryDto.getQuery(), giphyApiHelper.getGifURLFromGiphyApiSearch(userQueryDto.getQuery()));
    }

    private String finalizeGenerate(UserQueryDto userQueryDto, Path cachePath) throws IOException {
        String userId = userQueryDto.getUserId();
        String query = userQueryDto.getQuery();

        Path gifInUserDir = userGifRepository.formGifPath(userId, query, cachePath.getFileName().toString());
        if (!userGifRepository.checkIsPresent(gifInUserDir)) {
            gifInUserDir = userGifRepository.saveFromPath(userId, query, cachePath);
            userGifRepository.writeNewRecordToUsersHistory(userId, query, gifInUserDir);
        }

        String resourcePath = userGifRepository.formResourcePath(gifInUserDir);
        memoryCache.put(userId, query, resourcePath);
        return resourcePath;
    }

}
