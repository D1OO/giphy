package net.shvdy.bsagiphy.controller;

import net.shvdy.bsagiphy.service.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@RestController
@RequestMapping("/gifs")
public class GifsController {
    @Autowired
    GifService gifService;

    @GetMapping
    public Set<String> getAll() throws IOException {
        return gifService.getAll();
    }
}
