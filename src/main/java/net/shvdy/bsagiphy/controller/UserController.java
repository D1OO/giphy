package net.shvdy.bsagiphy.controller;

import lombok.extern.slf4j.Slf4j;
import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import net.shvdy.bsagiphy.dto.GenerateGifRequestDto;
import net.shvdy.bsagiphy.dto.UserHistoryRecordDto;
import net.shvdy.bsagiphy.dto.UserQueryDto;
import net.shvdy.bsagiphy.exception.HistoryIsEmptyException;
import net.shvdy.bsagiphy.exception.NoGifsFoundForQueryException;
import net.shvdy.bsagiphy.exception.ReceivedEmptyResponseException;
import net.shvdy.bsagiphy.service.GifGenerationService;
import net.shvdy.bsagiphy.service.GifSearchService;
import net.shvdy.bsagiphy.service.UserGifService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 04.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Validated
public class UserController {
    private static final String USER_ID_REGEX = "^[A-Za-z0-9_-]{1,20}$";
    private static final String QUERY_REGEX = "^[^<>%$?*:\"|/\\\\]*$";
    private UserGifService userGifService;
    private GifGenerationService gifGenerationService;
    private GifSearchService gifSearchService;

    public UserController(UserGifService userGifService,
                          GifGenerationService gifGenerationService, GifSearchService gifSearchService) {
        this.userGifService = userGifService;
        this.gifGenerationService = gifGenerationService;
        this.gifSearchService = gifSearchService;
    }

    @PostMapping("/{id}/generate")
    public String generateGif(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id,
                              @RequestBody @Valid GenerateGifRequestDto request)
            throws IOException, ReceivedEmptyResponseException, NoGifsFoundForQueryException {
        UserQueryDto userQueryDto = new UserQueryDto(id, request.getQuery());
        return (request.isForce()) ?
                gifGenerationService.generateGifFromApi(userQueryDto)
                :
                gifGenerationService.generateGifFromDiskCache(userQueryDto);
    }

    @GetMapping("/{id}/search")
    public String searchGif(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id,
                            @RequestParam @Pattern(regexp = QUERY_REGEX) String query,
                            @RequestParam Optional<String> force)
            throws IOException, NoGifsFoundForQueryException {

        UserQueryDto userQueryDto = new UserQueryDto(id, query);
        Optional<String> gifPath = (force.isPresent()) ?
                gifSearchService.searchInUsersFolder(userQueryDto)
                :
                gifSearchService.searchFromMemoryCache(userQueryDto);

        return gifPath.orElseThrow(() ->
                new NoGifsFoundForQueryException(String.format("Nothing found for query:%s for id:%s", query, id)));
    }

    @GetMapping("/{id}/all")
    public Set<AllGifsForQueryDto> getAll(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id)
            throws IOException {
        return userGifService.getAll(id);
    }

    @GetMapping("/{id}/history")
    public List<UserHistoryRecordDto> getHistory(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id)
            throws IOException, HistoryIsEmptyException {
        return userGifService.getHistory(id).orElseThrow(() ->
                new HistoryIsEmptyException("Requested history for id" + id + "but it's empty"));
    }

    @DeleteMapping("/{id}/history/clean")
    public ResponseEntity<Object> historyClean(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id) {
        userGifService.cleanHistory(id);
        return ResponseEntity.status(204).build();
    }

    @DeleteMapping("/{id}/clean")
    public ResponseEntity<Object> cleanAll(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id)
            throws IOException {
        userGifService.cleanAll(id);
        return ResponseEntity.status(204).build();
    }

    @DeleteMapping("/{id}/reset")
    public ResponseEntity<Object> resetMemoryCache(@PathVariable @Pattern(regexp = USER_ID_REGEX) String id,
                                                   @RequestParam(required = false)
                                                   @Pattern(regexp = QUERY_REGEX) String query) {
        if (Optional.ofNullable(query).isEmpty()) {
            userGifService.resetCache(id);
        } else {
            userGifService.resetCache(id, query);
        }
        return ResponseEntity.status(204).build();
    }

    @ExceptionHandler({NoGifsFoundForQueryException.class, HistoryIsEmptyException.class})
    public ResponseEntity<Object> handleNoGifFoundException(Exception e) {
        log.info("\n" + e.getMessage());
        return ResponseEntity.notFound().build();
    }

}
