package net.shvdy.bsagiphy.controller;

import net.shvdy.bsagiphy.dto.AllGifsForQueryDto;
import net.shvdy.bsagiphy.dto.QueryInBodyDto;
import net.shvdy.bsagiphy.exception.NoGifsFoundForQueryException;
import net.shvdy.bsagiphy.exception.ReceivedEmptyResponseException;
import net.shvdy.bsagiphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 **/

@RestController
@RequestMapping("/cache")
@Validated
public class CacheController {
    private static final String QUERY_REGEX = "^[^<>%$?*:\"|/\\\\]*$";
    @Autowired
    CacheService cacheService;

    @GetMapping
    public Set<AllGifsForQueryDto> get(@RequestParam(required = false) @Pattern(regexp = QUERY_REGEX) String query)
            throws IOException {
        if (Optional.ofNullable(query).isPresent()) {
            return cacheService.getAll(query);
        } else {
            return cacheService.getAll();
        }
    }

    @PostMapping("/generate")
    public AllGifsForQueryDto generate(@RequestBody @Valid QueryInBodyDto query)
            throws NoGifsFoundForQueryException, IOException, ReceivedEmptyResponseException {
        return cacheService.generate(query.getQuery());
    }

    @DeleteMapping
    public ResponseEntity<Object> deleteCache() throws IOException {
        cacheService.deleteAll();
        return ResponseEntity.status(204).build();
    }

}
