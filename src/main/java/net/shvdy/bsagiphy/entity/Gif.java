package net.shvdy.bsagiphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

/**
 * 05.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class Gif {
    String userId;
    String query;
    URL urlOnGiphy;
}
