package net.shvdy.bsagiphy.exception;

/**
 * 07.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
public class HistoryIsEmptyException extends Exception {
    public HistoryIsEmptyException(String message) {
        super(message);
    }
}
