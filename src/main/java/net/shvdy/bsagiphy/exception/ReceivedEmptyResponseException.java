package net.shvdy.bsagiphy.exception;

/**
 * 05.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
public class ReceivedEmptyResponseException extends Exception {
    public ReceivedEmptyResponseException(String message) {
        super(message);
    }
}
