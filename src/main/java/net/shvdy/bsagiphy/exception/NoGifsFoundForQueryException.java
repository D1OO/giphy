package net.shvdy.bsagiphy.exception;

/**
 * 05.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
public class NoGifsFoundForQueryException extends Exception {
    public NoGifsFoundForQueryException(String message) {
        super(message);
    }
}
