package net.shvdy.bsagiphy.config;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 06.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Component
public class MemoryCache {
    private ConcurrentHashMap<String, ConcurrentHashMap<String, Set<String>>> cache =
            new ConcurrentHashMap<>();
    private Random rnd = new Random();

    public void put(String userId, String query, String gifPath) {
        if (cache.containsKey(userId)) {
            Map<String, Set<String>> userQueries = cache.get(userId);
            if (userQueries.containsKey(query)) {
                userQueries.get(query).add(gifPath);
            } else {
                Set<String> newPath = ConcurrentHashMap.newKeySet();
                newPath.add(gifPath);
                userQueries.put(query, newPath);
            }
        } else {
            Set<String> newPath = ConcurrentHashMap.newKeySet();
            newPath.add(gifPath);
            ConcurrentHashMap<String, Set<String>> queries = new ConcurrentHashMap<>();
            queries.put(query, newPath);
            cache.put(userId, queries);
        }
    }

    public Optional<String> get(String userId, String query) {
        if (cache.containsKey(userId)) {
            Map<String, Set<String>> userQueries = cache.get(userId);
            if (userQueries.containsKey(query) && !userQueries.get(query).isEmpty()) {
                return userQueries.get(query).stream()
                        .skip(rnd.nextInt(userQueries.get(query).size()))
                        .findFirst();
            }
        }
        return Optional.empty();
    }

    public void deleteUser(String userId) {
        cache.remove(userId);
    }

    public void deleteUserQuery(String userId, String query) {
        if (cache.containsKey(userId)) {
            cache.get(userId).remove(query);
        }
    }

}
