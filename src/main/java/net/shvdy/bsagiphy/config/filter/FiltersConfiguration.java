package net.shvdy.bsagiphy.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

/**
 * 05.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Configuration
@Slf4j
public class FiltersConfiguration {

    @Bean
    public Filter loggingFilter() {
        AbstractRequestLoggingFilter loggingFilter = new AbstractRequestLoggingFilter() {

            @Override
            protected void beforeRequest(HttpServletRequest request, String message) {
                log.info(message);
            }

            @Override
            protected void afterRequest(HttpServletRequest httpServletRequest, String s) {
            }
        };

        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setIncludeQueryString(true);

        return loggingFilter;
    }
}